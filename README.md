## Ecosystem backlog refinement

#### Set up

Clone this repository, and install the gems:

```bash
bundle
```

#### API token

The GitLab Bot API token is in the 1Password Engineering vault as a
Secure Note titled _Ecosystem Backlog Refinement GitLab Bot API Token_.

#### Running

You can test what will happen with the `--dry-run` option:

```bash
bundle exec gitlab-triage --dry-run --token <api-token> --source-id gitlab-org/gitlab
```

Then to create the issue run the above without `--dry-run`.

#### Editing

See the `.triage-policies.yml` file and [`gitlab-triage` docs](https://gitlab.com/gitlab-org/gitlab-triage).
